// Nexus Mods: Automatic downloads
// Also: Utils for DOM ready & query string

/*
@TODO
- Add notice when automatic download is triggered
*/

/* global $ */

class Muffin_VariableChecks
{
	arrayIsValid( checkArr, debugLog = false )
	{
		let argIsValid = true;
		let debugMsg = '';

		if ( argIsValid && !checkArr )
		{
			argIsValid = false;
			debugMsg = 'Supplied array arg was undefined/null/false';
		}

		if ( argIsValid && !Array.isArray( checkArr ) )
		{
			argIsValid = false;
			debugMsg = `Expected array but supplied arg type was: ${typeof checkArr}`;
		}

		if ( argIsValid && checkArr.length < 1 )
		{
			argIsValid = false;
			debugMsg = 'Supplied array was empty';
		}

		// Debug Log
		if ( !argIsValid && debugLog )
		{
			console.log( `[arrayIsValid] Validation fail: ${debugMsg}` );
		}

		return argIsValid;
	}

	objectIsValid( checkObj, debugLog = false )
	{
		let argIsValid = true;
		let debugMsg = '';

		if ( argIsValid && !checkObj )
		{
			argIsValid = false;
			debugMsg = 'Supplied array arg was undefined/null/false';
		}

		if ( argIsValid && typeof checkObj !== 'object' )
		{
			argIsValid = false;
			debugMsg = `Expected object but supplied arg type was: ${typeof checkArr}`;
		}

		if ( argIsValid && !Object.keys( checkObj ).length )
		{
			argIsValid = false;
			debugMsg = 'Supplied object was empty';
		}

		// Debug Log
		if ( !argIsValid && debugLog )
		{
			console.log( `[objectIsValid] Validation fail: ${debugMsg}` );
		}

		return argIsValid;
	}

	objectContainsAllKeys( checkObj = {}, checkKeysArr = [], debugLog = false )
	{
		// Validation block
		const debugMsgLabel = '[objectContainsAllKeys]';

		if ( !this.objectIsValid( checkObj, debugLog ) )
		{
			if ( debugLog )
			{
				console.log( `${debugMsgLabel} Validation error for 1st arg (object), was supplied:`, checkObj );
			}

			return false;
		}

		if ( !this.arrayIsValid( checkKeysArr, debugLog ) )
		{
			if ( debugLog )
			{
				console.log( `${debugMsgLabel} Validation error for 2nd arg (array), was supplied:`, checkKeysArr );
			}

			return false;
		}

		// Actual check
		let containsKey = true;
		let checkKey = '';

		for ( let arrInd = 0; arrInd < checkKeysArr.length; arrInd++ )
		{
			checkKey = checkKeysArr[arrInd];

			if ( !Object.prototype.hasOwnProperty.call( checkObj, checkKey ) )
			{
				containsKey = false;
				break;
			}
		}

		return containsKey;
	}
}

class Muffin_QueryString
{
	parseQueryStringToObject( debugLog = false )
	{
		let queryString = '';
		let queryStringPairsArr = [];
		let queryStringSplitArr = [];
		let queryStringSplitObj = [];

		queryString = window.location.search;

		// Ignore empty, or just "?"
		if ( queryString.length < 2 )
		{
			return null;
		}

		queryString = ( queryString.startsWith( '?' ) ) ? queryString.substr( 1, queryString.length ) : '';

		queryStringPairsArr = queryString.split( '&' );

		queryStringPairsArr.forEach( pair =>
		{
			const splitPair = pair.split( '=' );

			if ( splitPair.length )
			{
				const pairKey = splitPair[0];
				const pairVal = splitPair[1];

				queryStringSplitArr.push( splitPair ); // UNUSED

				queryStringSplitObj[pairKey] = pairVal;
			}
		} );

		if ( debugLog )
		{
			console.log( { queryString, queryStringPairsArr, queryStringSplitArr, queryStringSplitObj } );
		}

		return queryStringSplitObj;
	}

	queryStringContainsAllKeys( keysArr = [], debugLog = false )
	{
		// Parsed query string object
		let qsObj = this.parseQueryStringToObject( debugLog );

		if ( !qsObj )
		{
			return;
		}

		const VC = new Muffin_VariableChecks();

		return VC.objectContainsAllKeys( qsObj, keysArr, debugLog );
	}
}


class Muffin_DOMTools
{
	// via https://learnwithparam.com/blog/vanilla-js-equivalent-of-jquery-ready/
	ready(callbackFunc)
	{
		if (document.readyState !== 'loading')
		{
			// Document is already ready, call the callback directly
			callbackFunc();
		}
		else if (document.addEventListener)
		{
			// All modern browsers to register DOMContentLoaded
			document.addEventListener('DOMContentLoaded', callbackFunc);
		}
		else
		{
			// Old IE browsers
			document.attachEvent('onreadystatechange', function()
			{
				if (document.readyState === 'complete')
				{
					callbackFunc();
				}
			});
		}
	}
}


class Muffin_NexusDownloads
{
	constructor()
	{
		const MQS            = new Muffin_QueryString();
		const queryStringObj = MQS.parseQueryStringToObject();

		this.debugLog        = true;
		this.classLabel      = 'Muffin_NexusDownloads';
		this.MQS             = MQS;
		this.queryStringObj  = queryStringObj;
		this.file_id         = ( !MQS.queryStringContainsAllKeys( [ 'file_id' ] ) )                   ? null : this.queryStringObj['file_id'];
		this.game_id         = ( !Object.prototype.hasOwnProperty.call( window, 'current_game_id' ) ) ? null : window.current_game_id;
	}

	init()
	{
		const funcLabel = 'init';

		if ( this.isDownload() )
		{
			// Validation: Check global vars
			if ( !this.file_id || !this.game_id )
			{
				this.logMessage( 'error', funcLabel, 'Global vars "file_id" and/or "game_id" were not available' );
				return;
			}

			if ( this.isVortexDownload() )
			{
				this.processDownloadVortex();
			}
			else
			{
				this.processDownloadManual();
			}
		}
	}


	// Download Types
	// ============================================================================

	isDownload()
	{
		const keysArr = [ 'tab', 'file_id' ];

		return this.MQS.queryStringContainsAllKeys( keysArr, this.debugLog );
	}

	isVortexDownload()
	{
		// Array of keys to check the query string for
		const keysArr = [ 'tab', 'file_id', 'nmm' ];

		return this.MQS.queryStringContainsAllKeys( keysArr, this.debugLog );
	}


	// Process Downloads
	// ============================================================================

	processDownloadVortex()
	{
		console.log('processDownloadVortex start');

		const funcLabel = 'processDownloadVortex';

		// Get URL from download button
		const downloadBtn = document.getElementById( 'slowDownloadButton' );

		if ( !downloadBtn )
		{
			this.logMessage( 'error', funcLabel, 'On download page, but download button is not present' );
			return;
		}

		const vortexURI = downloadBtn.getAttribute( 'data-download-url' );

		window.location.href = vortexURI;

		this.showDownloadNotice( 'successVortex', vortexURI );
	}

	processDownloadManual()
	{
		const funcLabel = 'processDownload';

		$.ajax( {
			type: 'POST',
			url: '/Core/Libs/Common/Managers/Downloads?GenerateDownloadUrl',
			data: {
				fid:     this.file_id,
				game_id: this.game_id,
			},
			success: data =>
			{
				if ( data && data.url )
				{
					this.logMessage( 'info', funcLabel, 'AJAX success. URL is:', data.url );
					this.showDownloadNotice( 'successManual', data.url );

					window.location.href = data.url;
				}
				else
				{
					this.logMessage( 'error', funcLabel, 'AJAX error', 'Requested data was not available' );
					this.showDownloadNotice( 'error' );
				}
			},
			error: function ( err )
			{
				this.logMessage( 'error', funcLabel, 'AJAX error: A general error occured', err );
				this.showDownloadNotice( 'error' );
			}
		} );
	}


	// Misc
	// ============================================================================

	logMessage( type = 'log', label = '', message = '', data = null )
	{
		console[type]( `[Muffin_NexusDownloads:${label}] ${message}`, data );
	}

	/**
	 * Set element visibility with inline styles (like vanilla Nexus does)
	 *
	 * @param    {string}   selector     Target selector string
	 * @param    {boolean}  [show=true]  True to show, false to hide
	 * @return   {void}
	 */
	setElementVisibilityInline( selector, show = true )
	{
		const el = document.querySelector( selector );

		if ( el )
		{
			if ( show )
			{
				el.style.display = 'block';
			}
			else
			{
				el.style.display = 'none';
			}
		}
		else
		{
			this.logMessage( 'error', 'setElementVisibilityInline', `Element not found for selector "${selector}"` );
		}
	}

	showDownloadNotice( noticeType = null, downloadUrl = '#' )
	{
		// Hide download info, show notice container
		this.setElementVisibilityInline( '.subheader', false );
		this.setElementVisibilityInline( '.table',     false );
		this.setElementVisibilityInline( '.donation-wrapper', true );

		// Update text
		let newHtml = '';

		switch ( noticeType )
		{
			case 'successManual':
				newHtml = `
					<p>Your download has started</p>
					<p>If you are having trouble, <a href="${downloadUrl}">click here</a> to download manually</p>
				`;
				break;

			case 'successVortex':
				newHtml = `
					<p>Vortex will now download & install this mod</p>
					<p>If you are having trouble, <a href="${downloadUrl}">click here</a> to open this mod in Vortex manually</p>
					<p class="vortex-download-note">
						Vortex will launch automatically if it is not already running.
					</p>
				`;
				break;

			case 'error':
				newHtml = `
					<p>Unfortunately an error occurred while downloading this file</p>
					<p>Please try again later or contact support</p>
				`;
				break;
		}

		$('.donation-wrapper > p').html( newHtml );
	}
}


class Muffin_NexusTitle
{
	// EXAMPLE TITLES
	// Single mod:     Map at Subnautica Nexus - Mods and community
	// Mod categories: Subnautica mod categories at Subnautica Nexus - Mods and community
	// Mod category:   Mod categories at Subnautica Nexus - Mods and community

	init()
	{
		this.replaceTitle();
	}

	replaceTitle()
	{
		document.title = this.getH1Text();

		// const gameName = this.getGameName();
		// if ( gameName ) {}
	}

	getH1Text()
	{
		return document.querySelector( 'h1' ).innerText;
	}

	/*
	getGameName()
	{
		const gameNameEl = document.querySelector( '.rj-nav-game-home .game-name' );

		if ( !gameNameEl )
		{
			return null;
		}

		return gameNameEl.getAttribute( 'title' );
	}
	*/
}

class Muffin_NexusTweaks
{
	init()
	{
		const NexusDownloads = new Muffin_NexusDownloads();
		const NexusTitle     = new Muffin_NexusTitle();

		NexusDownloads.init();
		NexusTitle.init();
	}
}


const MDT = new Muffin_DOMTools();

MDT.ready( () =>
{
	const MN = new Muffin_NexusTweaks();
	MN.init();
} );
