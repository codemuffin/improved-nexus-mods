# Improved Nexus Mods

The foundation for a WIP chrome extension for [Nexus Mods](https://nexusmods.com).

You can use this stuff now with the excellt Chrome extension, [User JavaScript and CSS](https://chrome.google.com/webstore/detail/user-javascript-and-css/nbhcbdghjpllgmfilhnhkllmkecfmpld).

**CSS Features:**

- Various fixes for the text editor, incl. better text color previews

**JS Features:**

- Automatic downloads
- Cleaner `<title>` elements for tidier bookmarks (removes game name)

The code also has utils for variable validation and query string parsing.
